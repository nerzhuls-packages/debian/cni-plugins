#! /bin/bash

set -e
set -u

PKG_ARCH=${1}

for PKG in cni-plugins; do
    echo "Packaging ${PKG}"
    mkdir -p ${PKG}/opt/cni/bin/
    cd ${PKG}/opt/cni/bin/
    tar xvzf ../../../../build/cni-plugins-linux-${PKG_ARCH}-v${CNI_PLUGINS_VERSION}.tgz
    cd -
    # Replace versions
    sed -i 's/%%CNI_PLUGINS_VERSION%%/'${CNI_PLUGINS_PACKAGE_VERSION}'/g' ${PKG}/DEBIAN/control
    sed -i 's/amd64/'${PKG_ARCH}'/g' ${PKG}/DEBIAN/control
    dpkg-deb -b ${PKG}
    mv ${PKG}.deb dist/${PKG}-${PKG_ARCH}.deb
done
